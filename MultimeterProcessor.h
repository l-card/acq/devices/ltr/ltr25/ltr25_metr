#ifndef MULTIMETERPROCESSOR_H
#define MULTIMETERPROCESSOR_H

#include "MultimeterThread.h"

class MultimeterProcessor : public QObject {
    Q_OBJECT
public Q_SLOTS:
    void processData(LQMeas::DevAdc *adc, QVector<double> vals, MultimeterThread::MultimeterData data);
Q_SIGNALS:
    void dataProcessed(MultimeterThread::MultimeterData data);
};

#endif // MULTIMETERPROCESSOR_H
