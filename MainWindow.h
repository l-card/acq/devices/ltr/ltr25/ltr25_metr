#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QLabel>

#include "SettingsDialog.h"
#include "lqmeas/devs/LTR/modules/LTR25/LTR25.h"
#include "lqmeas/devs/LTR/modules/LTR25/LTR25Config.h"
#include "MultimeterThread.h"

class QGridLayout;
class QCheckBox;
class QLedIndicator;
class LTRModuleSelectionUI;


namespace LQMeas {
    class LogPanel;
}

namespace Ui {
    class MainWindow;
}



class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


public slots:
    void reset();



private slots:
    void closeCurModule();
    void onBeforeModuleChanged();
    void onModuleChanged();


    void multShowSettingDialog();

    void multConnectSignals(bool en);
    void setManualUi();
    void multUpdateUi();

    void multGetConfig(LQMeas::LTR25Config &cfg);
    void multSetConfig();
    void multCheckConfig();
    void multConfigChanged();
    void multClearMeas();
    void multShowResult(MultimeterThread::MultimeterData data, bool blink=true);
    void multShowNewResult(MultimeterThread::MultimeterData data);
    void multThreadFinished();


    void allChEnChanged(int state);


    void saveSettings();
    void setSelectedText(QLineEdit *edt, QString text);



protected:
    void closeEvent ( QCloseEvent * event );
private:    
    QSharedPointer<LQMeas::LTR25> curDev();


    Ui::MainWindow *ui;
    struct ChUi {
        QCheckBox *enBox;
        QLineEdit *dcVal;
        QLineEdit *acVal;
        QLineEdit *freq;
        QLineEdit *snr;
        QLedIndicator *led;
    };

    ChUi createChannelUi(QGridLayout *lout, QString name, int row);
    QLineEdit* createDigIndicator();

    LTRModuleSelectionUI *m_devsel;
    MultimeterThread   *m_mult;
    MultSettings        m_multSet;

    LQMeas::LogPanel *m_logPanel;

    int m_mult_cfg_id;
    MultimeterThread::MultimeterData m_last_data;
    bool m_last_data_valid;

    QList<ChUi> m_chUi;
};

#endif // MAINWINDOW_H
