#include "SettingsDialog.h"
#include "ui_SettingsDialog.h"

MultSettingsDialog::MultSettingsDialog(MultSettings initSet, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog) {

    ui->setupUi(this);

    ui->prec_dc->setValue(initSet.dc_prec);
    ui->prec_ac->setValue(initSet.ac_prec);
    ui->precFreq->setValue(initSet.freq_prec);
    ui->precSNR->setValue(initSet.snr_prec);
}

MultSettingsDialog::~MultSettingsDialog() {
    delete ui;
}

MultSettings MultSettingsDialog::settings() const {
    MultSettings set;
    set.dc_prec = ui->prec_dc->value();
    set.ac_prec = ui->prec_ac->value();
    set.freq_prec = ui->precFreq->value();
    set.snr_prec = ui->precSNR->value();

    return set;
}
