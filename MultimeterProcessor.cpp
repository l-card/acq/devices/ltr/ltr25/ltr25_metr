#include "MultimeterProcessor.h"
#include "lqmeas/ifaces/in/DevAdcConfig.h"
#include "lqmeas/log/Log.h"
#include "lmath.h"


void MultimeterProcessor::processData(LQMeas::DevAdc *adc, QVector<double> vals, MultimeterThread::MultimeterData data) {
    int enabled_ch_cnt = adc->adcConfig()->adcEnabledChCnt();
    int ch_size = vals.size()/enabled_ch_cnt;
    int fft_size = ch_size/2 + 1;

    QScopedArrayPointer<double> ch_vals(new double[ch_size]);
#ifndef LQMEAS_SERVICE_MULTIMETER_DC_ONLY
    QScopedArrayPointer<double> fft(new double[fft_size]);
    QScopedArrayPointer<double> scaled(new double[ch_size]);

    double df = 0.0;
    double dt = 1./adc->adcConfig()->adcFreq();
#endif

    for (int ch=0; ch < enabled_ch_cnt; ch++) {
        unsigned ch_num = adc->adcChNum(ch);

        if ((data.ch[ch_num].status == LQMeas::DevAdc::ChStatusOk) ||
                (data.ch[ch_num].status == LQMeas::DevAdc::ChStatusOverload)) {
            for (int i=0; i < ch_size; i++) {
                ch_vals[i] = vals[enabled_ch_cnt*i + ch];
            }

            int ch_err=0;
#ifdef LQMEAS_SERVICE_MULTIMETER_DC_ONLY
            if (ch_size==1) {
                data.ch[ch_num].DC = ch_vals[0];
            } else {
                ch_err = lmath_acdc_estimation(ch_vals.data(), ch_size,
                                               &data.ch[ch_num].DC, 0);
            }
#else
            ch_err = lmath_acdc_estimation(ch_vals.data(), ch_size, &data.ch[ch_num].DC,
                                           &data.ch[ch_num].AC);
            if (!ch_err) {
                t_lmath_window_ctx winctx = lmath_window_ctx_create(LMATH_WINTYPE_BH_4TERM, nullptr, ch_size);
                ch_err = lmath_window_apply_scaled(ch_vals.data(), winctx, scaled.data());
                if (!ch_err) {
                    ch_err = lmath_amp_pha_spectrum_rms(
                                scaled.data(), ch_size, dt, 0, LMATH_PHASE_MODE_COS,
                                fft.data(), nullptr, &df);
                }
                if (!ch_err) {
                    ch_err = lmath_spectrum_find_peak_freq(fft.data(), fft_size,
                                                           df, lmath_window_params(winctx),
                                                           -1, &data.ch[ch_num].freq, nullptr);
                }
                if (!ch_err) {
                    t_lmath_signoise_result sigres;
                    if (lmath_spectrum_calc_signoise(fft.data(), fft_size, df,
                                                     lmath_window_params(winctx),
                                                     data.ch[ch_num].freq, 6,
                                                     LMATH_SIGNOISE_FLAGS_CALC_SNR | LMATH_SIGNOISE_FLAGS_CALC_SINAD,
                                                     &sigres) != 0) {
                        data.ch[ch_num].SNR = 0;
                        data.ch[ch_num].SINAD = 0;
                    } else {
                        data.ch[ch_num].SNR = sigres.params.SNR;
                        data.ch[ch_num].SINAD = sigres.params.SNR;
                    }
                }
                lmath_window_ctx_destroy(winctx);
            }
#endif

            if (ch_err) {
                data.ch[ch_num].status = LQMeas::DevAdc::ChStatusError;
                LQMeasLog->error(tr("Cannot process data block"), LQMeas::StdErrors::DataProcess());
            }
        }
    }
    Q_EMIT dataProcessed(data);
}
