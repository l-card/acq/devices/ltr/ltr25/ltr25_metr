#ifndef MULTIMETERTHREAD_H
#define MULTIMETERTHREAD_H


#include <QThread>
#include <QVector>
#include <QMutex>
#include <QSharedPointer>
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "lqmeas/ifaces/in/DevAdc.h"


class MultimeterProcessor;

   class MultimeterThread : public QThread {
       Q_OBJECT
   public:

       struct MultimeterChData {
           LQMeas::DevAdc::ChannelStatus status;
           double DC;
           double AC;
           double freq;
           double SNR;
           double SINAD;
       };

       struct MultimeterData {
           int cfgID;
           QVector<MultimeterChData> ch;
       };

       explicit MultimeterThread(QObject *parent = nullptr);

       void setDev(QSharedPointer<LQMeas::DevAdc> adc);



       void run() override;
       LQMeas::Error lastError() const {return m_err;}

       /* Включение старт-стопного режима. В этом режиме по приему блока
        * поток генерирует сигнал dataForProcess() с обработанными данными
        * и останавливает сбор до вызова blockProcessDone() */
       void setStartStopMode(bool en) {m_startStopMode = en;}
   Q_SIGNALS:
       void dataReceived(MultimeterThread::MultimeterData data);
       void dataForProcess(LQMeas::DevAdc *adc, QVector<double> vals, MultimeterThread::MultimeterData data);
   public Q_SLOTS:
       void setConfig(QSharedPointer<LQMeas::DeviceConfig> cfg, int meas_time, int *id);
       void stopRequest();
       void blockProcessDone() {m_processDone = true;}
   private:
       bool restartReq() {return m_stopReq || m_cfgReq;}
       mutable QMutex m_lock;
       LQMeas::Error m_err;
       QSharedPointer<LQMeas::Device>  m_dev;
       LQMeas::DevAdc *m_adc;
       QSharedPointer<LQMeas::DeviceConfig> m_curCfg;
       size_t m_cfgSize;

       int m_meas_time;
       bool m_startStopMode;
       bool m_processDone;
       bool m_stopReq;
       bool m_cfgReq;
       int m_cur_id;
       MultimeterProcessor *m_proc;
       QThread m_procThread;
   };

#endif // MULTIMETERTHREAD_H
