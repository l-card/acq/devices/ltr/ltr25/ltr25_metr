#include <QApplication>
#include "MainWindow.h"
#include "QtTranslationSwitch.h"
#ifdef Q_OS_WIN
    #include <QSettings>
#endif

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
#ifdef Q_OS_WIN
    /* на Windows используем ini-файлы, а не реестр (как по умолчанию) */
    QSettings::setDefaultFormat(QSettings::IniFormat);
#endif

    a.setOrganizationName("L Card");
    a.setApplicationName(PROJECT_NAME);
    a.setOrganizationDomain("lcard.ru");
    a.setApplicationVersion(PRJ_VERION_STR);

    QtTranslationSwitch *ts = QtTranslationSwitch::instance();
    ts->init(PRJ_TRANSLATIONS_DIR);
    ts->addTranslationFile("lqmeas");
    ts->setLanguage(QtTranslationSwitch::langNameRu());

    MainWindow w;
    w.show();
    w.reset();

    return a.exec();

}
