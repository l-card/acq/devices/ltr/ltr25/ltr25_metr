#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QSettings>
#include <QMessageBox>
#include <QProgressDialog>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QCheckBox>
#include <QCoreApplication>
#include <QtDebug>
#include <QLineEdit>
#include <QPushButton>

#include "lqmeas/log/Log.h"
#include "lqmeas/log/LogPanel.h"
#include "QLedIndicator.h"



#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/devs/LTR/LTRResolver.h"
#include "lqmeas/devs/LTR/modules/LTR25/LTR25Info.h"
#include "LTRModuleSelectionUI.h"
#include "MultimeterThread.h"

#include "WaitDialog.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), m_last_data_valid(false) {

    ui->setupUi(this);
    setWindowTitle(QCoreApplication::applicationName() + " - " + QCoreApplication::applicationVersion());

/* загрузка настроек состояния интерфейса */
#ifdef Q_WS_WIN
    /* на Windows используем ini-файлы, а не реестр (как по умолчанию) */
    QSettings::setDefaultFormat(QSettings::IniFormat);
#endif

    QSettings set;
    set.beginGroup("MainWindow");
    restoreState(set.value("state").toByteArray());
    resize(set.value("size", QSize(800, 600)).toSize());
    move(set.value("pos", QPoint(0, 0)).toPoint());
    set.endGroup();


    setManualUi();


    set.beginGroup("Multimeter");
    ui->adcFreq->setCurrentIndex(set.value("adc_freq", 0).toInt());
    ui->format->setCurrentIndex(set.value("adc_fmt", 0).toInt());
    ui->measTime->setValue(set.value("meas_time", 1000).toInt());
    ui->iSrcValue->setCurrentIndex(set.value("i_src_val", 0).toInt());
    ui->icpPhaseCor->setChecked(set.value("icp_phase_cor", true).toBool());

    set.beginReadArray("channels");
    for (int ch = 0; ch < m_chUi.size(); ch++) {
        set.setArrayIndex(ch);
        m_chUi[ch].enBox->setChecked(set.value("en", ch==0).toBool());
    }
    set.endArray();

    set.beginGroup("precision");
    m_multSet.dc_prec = set.value("dc", 4).toInt();
    m_multSet.ac_prec = set.value("ac", 4).toInt();
    m_multSet.freq_prec = set.value("freq", 3).toInt();
    m_multSet.snr_prec = set.value("snr", 3).toInt();
    set.endGroup();

    set.beginGroup("show");
    ui->ch_dc_show->setChecked(set.value("ch_dc", true).toBool());
    ui->ch_ac_show->setChecked(set.value("ch_ac", true).toBool());
    ui->ch_freq_show->setChecked(set.value("ch_freq", true).toBool());
    ui->ch_snr_show->setChecked(set.value("ch_snr", true).toBool());
    set.endGroup();

    set.endGroup();


    m_mult = new MultimeterThread();
    m_mult->setStartStopMode(true);
    m_mult->moveToThread(m_mult);
    connect(m_mult, &MultimeterThread::dataReceived,
            this, &MainWindow::multShowNewResult,
            Qt::QueuedConnection);
    connect(m_mult, &MultimeterThread::finished,
            this, &MainWindow::multThreadFinished);

    connect(ui->resetButton, &QPushButton::pressed, this, &MainWindow::reset);
    connect(ui->multSetButton, &QPushButton::pressed, this, &MainWindow::multShowSettingDialog);

    m_logPanel = new LQMeas::LogPanel(this);
    addDockWidget(Qt::BottomDockWidgetArea, m_logPanel);

    m_devsel = new LTRModuleSelectionUI(QStringList() << LQMeas::LTR25TypeInfo::defaultInfo()->deviceTypeName(),
                                      ui->ltr25_crate, ui->ltr25_slot, ui->ltr25_serial,
                                      this);

    connect(m_devsel, &LTRModuleSelectionUI::beforeModuleChange, this, &MainWindow::onBeforeModuleChanged);
    connect(m_devsel, &LTRModuleSelectionUI::moduleChanged, this, &MainWindow::onModuleChanged);



    m_mult = new MultimeterThread();
    m_mult->moveToThread(m_mult);
    connect(m_mult, &MultimeterThread::dataReceived,
            this, &MainWindow::multShowNewResult,
            Qt::QueuedConnection);
    connect(m_mult, &MultimeterThread::finished,
            this, &MainWindow::multThreadFinished);



    multCheckConfig();
    multUpdateUi();
    multConnectSignals(true);

    LQMeasLog->info(tr("Приложение запущено"));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::reset() {
    LQMeasLog->info("Сброс программы...");
    closeCurModule();
    m_devsel->reset();
}

void MainWindow::onBeforeModuleChanged() {
    closeCurModule();
}

void MainWindow::onModuleChanged() {
    QSharedPointer<LQMeas::LTR25> dev = curDev();

    if (dev) {
        const LQMeas::LTR25Info *info = dev->devspecInfo();

        ui->ltr25_serial->setText(info->serial());
        ui->ltr25_verPLD->setText(info->pldVerStr());
        ui->ltr25_verFPGA->setText(info->fpgaVerStr());
        ui->ltr25_ipcExtBw->setChecked(info->icpExtendedBandwidthLF());



        m_mult->setDev(dev);
        multUpdateUi();
        multSetConfig();
        m_mult->start();
    } else {
        ui->ltr25_serial->clear();
        ui->ltr25_verPLD->clear();
        ui->ltr25_verFPGA->clear();
        ui->ltr25_ipcExtBw->setChecked(false);
    }
}

void MainWindow::closeCurModule() {
    if (m_mult->isRunning()) {
        WaitDialog dialog(this);

        LQMeasLog->info(tr("Останавливаем измерения..."));
        dialog.setModal(true);
        dialog.show();

        /* останавливаем сбор мультиметра */
        m_mult->stopRequest();
        bool stopped = false;
        do {
            QCoreApplication::processEvents();
            stopped = m_mult->wait(100);
        } while (!stopped);
    }
    m_devsel->closeCurModule();
}

void MainWindow::multShowSettingDialog() {
    MultSettingsDialog dialog(m_multSet, this);
    if (dialog.exec()==QDialog::Accepted)  {
        m_multSet=dialog.settings();
        if (m_last_data_valid)
            multShowResult(m_last_data, false);
    }
}

void MainWindow::multThreadFinished() {
    /* проверяем, завершился ли сбор из-за ошибки */
    if (!m_mult->lastError().isSuccess()) {
        closeCurModule();
        QMessageBox::critical(this, tr("Ошибка сбора"), tr("Возникла ошибка при измерении: %1.\nВыбирите другой модуль или проверте подключение и нажмите 'Сброс'")
                              .arg(m_mult->lastError().msg()));
    }
}



void MainWindow::setManualUi() {
    QGridLayout *lout = ui->channelsLayout;
    int row = 1;

    for (unsigned ch=0; ch < LTR25_CHANNEL_CNT; ch++) {
        m_chUi.append(createChannelUi(lout, tr("Канал %1").arg(QString::number(ch+1)), row++));
    }
}

/* обновление доступности для изменения элементов интерфейса */
void MainWindow::multUpdateUi() {
    LQMeas::LTR25Config cfg;
    multGetConfig(cfg);
    Qt::CheckState enAllState;

    for (int ch=0; ch < m_chUi.size(); ch++) {
        if (ch==0) {
            enAllState = m_chUi[ch].enBox->checkState();
        } else if (m_chUi[ch].enBox->checkState()!=enAllState) {
            enAllState = Qt::PartiallyChecked;
        }
    }

    ui->enAllChBox->setCheckState(enAllState);
}



/* очистка текста на выводах мультиметра */
void MainWindow::multClearMeas() {
    m_last_data_valid = false;

    foreach (ChUi ch_ui, m_chUi) {
        ch_ui.dcVal->setText("");
        ch_ui.acVal->setText("");
        ch_ui.freq->setText("");
        ch_ui.snr->setText("");
    }
    QCoreApplication::processEvents();
}


void MainWindow::multGetConfig(LQMeas::LTR25Config &cfg) {
    cfg.adc().adcSetFreqCode(ui->adcFreq->currentIndex());
    cfg.adc().adcSetDataFormat(ui->format->currentIndex() == 0 ? LQMeas::LTR25AdcConfig::AdcDataFormat20 :
                                                                 LQMeas::LTR25AdcConfig::AdcDataFormat24);
    cfg.icp().adcSetISrcValue(ui->iSrcValue->currentIndex()==0 ? LQMeas::LTR25AdcICPConfig::ISrcValue_10mA :
                                                                 LQMeas::LTR25AdcICPConfig::ISrcValue_2_86mA);
    cfg.icp().adcSetPhaseCorEnabled(ui->icpPhaseCor->isChecked());

    for (int ch=0; ch < LTR25_CHANNEL_CNT; ch++) {
        cfg.adc().adcSetChEnabled(ch,  m_chUi[ch].enBox->isChecked());
    }
    cfg.update();
}

void MainWindow::multSetConfig() {
    QSharedPointer<LQMeas::LTR25Config> cfg(new LQMeas::LTR25Config());
    multGetConfig(*cfg);
    multClearMeas();
    int meas_time = ui->measTime->value();
    m_mult->setConfig(cfg, meas_time, &m_mult_cfg_id);
    multClearMeas();
}

void MainWindow::multCheckConfig() {
    LQMeas::LTR25Config cfg;
    int ch_cnt, max_ch_cnt;

    multGetConfig(cfg);

    multConnectSignals(false);

    max_ch_cnt = cfg.adc().adcMaxEnabledChCnt();
    ch_cnt = cfg.adc().adcEnabledChCnt();

    /* Должен быть разрешен хотя бы один канал! */
    if (ch_cnt==0) {
        ch_cnt = 1;
        QMessageBox::warning(this, tr("Ошибка настроек"),
                             tr("Не было разрешено ни одного канала!\nПервый канал будет резрешен принудительно."),
                             QMessageBox::Ok);

        m_chUi[0].enBox->setCheckState(Qt::Checked);
        cfg.adc().adcSetChEnabled(0, true);
        cfg.update();
    } else if (ch_cnt > max_ch_cnt) {
        QMessageBox::warning(this, tr("Ошибка настроек"),
                             tr("На данной частоте для выбранного формата количество одновременно работающих каналов должно быть не больше %1.\nОстальные каналы будут запрещены!").arg(QString::number(max_ch_cnt)),
                             QMessageBox::Ok);
        ch_cnt = 0;
        for (int ch=0; ch < LTR25_CHANNEL_CNT; ch++) {
            if (cfg.adc().adcChEnabled(ch)) {
                if (ch_cnt < max_ch_cnt) {
                    ch_cnt++;
                } else {
                    cfg.adc().adcSetChEnabled(ch, false);
                    m_chUi[ch].enBox->setCheckState(Qt::Unchecked);
                }
            }
        }
        cfg.update();
    }

    multConnectSignals(true);
}

void MainWindow::multConfigChanged() {
    multCheckConfig();
    multUpdateUi();
    multSetConfig();
}


void MainWindow::setSelectedText(QLineEdit* edt, QString text) {
    QString selText = edt->selectedText();
    QString oldText = edt->text();
    int sel_start  = edt->selectionStart();
    edt->setText(text);
    if (!selText.isEmpty()) {
        edt->setSelection(sel_start, selText.length() + text.length() - oldText.length());
    }
}

void MainWindow::multShowResult(MultimeterThread::MultimeterData data, bool blink) {
    if (data.cfgID== m_mult_cfg_id) {
        for (int ch=0; ch < m_chUi.size(); ch++) {
            if (data.ch[ch].status != LQMeas::DevAdc::ChStatusDisabled) {
                if (data.ch[ch].status ==  LQMeas::DevAdc::ChStatusOk) {
                    QLocale locale;

                    setSelectedText(m_chUi[ch].dcVal, ui->ch_dc_show->isChecked() ?
                                    locale.toString(data.ch[ch].DC, 'f', m_multSet.dc_prec) : "");
                    setSelectedText(m_chUi[ch].acVal, ui->ch_ac_show->isChecked() ?
                                        locale.toString(data.ch[ch].AC, 'f', m_multSet.ac_prec) : "");
                    setSelectedText(m_chUi[ch].freq, ui->ch_freq_show->isChecked() ?
                                    locale.toString(data.ch[ch].freq, 'f', m_multSet.freq_prec) : "");
                    setSelectedText(m_chUi[ch].snr, ui->ch_snr_show->isChecked() ?
                                    locale.toString(data.ch[ch].SNR, 'f', m_multSet.snr_prec) : "");

                } else {
                    QString msg;
                    switch (data.ch[ch].status) {
                        case LQMeas::DevAdc::ChStatusShort:
                            msg = "КЗ"; break;
                        case LQMeas::DevAdc::ChStatusOpen:
                            msg = "Обрыв"; break;
                        default:
                            msg = ""; break;
                    }

                    m_chUi[ch].dcVal->setText(ui->ch_dc_show->isChecked() ? msg : "");
                    m_chUi[ch].acVal->setText(ui->ch_ac_show->isChecked() ? msg : "");
                    m_chUi[ch].freq->setText(ui->ch_freq_show->isChecked() ? msg : "");
                    m_chUi[ch].snr->setText(ui->ch_snr_show->isChecked() ? msg : "");
                }

                if (blink)
                    m_chUi[ch].led->blink();
            }
        }

        m_last_data = data;
        m_last_data_valid = true;
    }
}

void MainWindow::multShowNewResult(MultimeterThread::MultimeterData data) {
    multShowResult(data, true);
}



MainWindow::ChUi MainWindow::createChannelUi(QGridLayout *lout, QString name, int row) {
    ChUi ch_ui;

    ch_ui.enBox = new QCheckBox(name);
    QFont font = ch_ui.enBox->font();
    font.setPointSize(14);
    ch_ui.enBox->setFont(font);
    lout->addWidget(ch_ui.enBox, row, 0);

    ch_ui.dcVal = createDigIndicator();
    lout->addWidget(ch_ui.dcVal, row, 1);

    ch_ui.acVal = createDigIndicator();
    lout->addWidget(ch_ui.acVal, row, 2);

    ch_ui.freq = createDigIndicator();
    lout->addWidget(ch_ui.freq,  row, 3);

    ch_ui.snr  = createDigIndicator();
    lout->addWidget(ch_ui.snr,   row, 4);

    ch_ui.led = new QLedIndicator();
    ch_ui.led->setEnabled(false);
    lout->addWidget(ch_ui.led,   row, 5);




    return ch_ui;
}

QLineEdit* MainWindow::createDigIndicator() {
    QLineEdit* edt = new QLineEdit();
    QFont font = edt->font();
    font.setPointSize(20);
    edt->setFont(font);

    edt->setMinimumWidth(185);
    edt->setStyleSheet("background-color: rgb(0, 0, 0); color: rgb(0, 170, 0); border: 3px solid green");
    edt->setReadOnly(true);
    edt->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    return edt;
}

void MainWindow::saveSettings() {
    QSettings set;
    set.beginGroup("MainWindow");
    /* если коно в настоящий момент не свернуто - то сохраняем его текущие
       параметры */
    if (this->isVisible() && !isMinimized()) {
        set.setValue("state", saveState());
        set.setValue("size", size());
        set.setValue("pos", pos());
    }
    set.endGroup();

    /* сохранение настроек мультиметра */
    set.beginGroup("Multimeter");
    set.setValue("adc_freq", ui->adcFreq->currentIndex());
    set.setValue("adc_fmt", ui->format->currentIndex());
    set.setValue("meas_time", ui->measTime->value());
    set.setValue("i_src_val", ui->iSrcValue->currentIndex());
    set.setValue("icp_phase_cor", ui->icpPhaseCor->isChecked());

    set.beginWriteArray("channels");
    for (int ch = 0; ch < m_chUi.size(); ch++) {
        set.setArrayIndex(ch);
        set.setValue("en", m_chUi[ch].enBox->isChecked());
    }
    set.endArray();


    set.beginGroup("precision");
    set.setValue("dc", m_multSet.dc_prec);
    set.setValue("ac", m_multSet.ac_prec);
    set.setValue("freq", m_multSet.freq_prec);
    set.setValue("snr", m_multSet.snr_prec);
    set.endGroup();

    set.beginGroup("show");
    set.setValue("ch_dc", ui->ch_dc_show->isChecked());
    set.setValue("ch_ac", ui->ch_ac_show->isChecked());
    set.setValue("ch_freq", ui->ch_freq_show->isChecked());
    set.setValue("ch_snr", ui->ch_snr_show->isChecked());
    set.endGroup();
    set.endGroup();
}



void MainWindow::multConnectSignals(bool en) {
    for (int ch=0; ch < m_chUi.size(); ch++) {
        if (en) {
            connect(m_chUi[ch].enBox, &QCheckBox::stateChanged, this, &MainWindow::multConfigChanged);
        } else {
            disconnect(m_chUi[ch].enBox, &QCheckBox::stateChanged, this, &MainWindow::multConfigChanged);
        }
    }

    if (en) {
        connect(ui->adcFreq,   static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MainWindow::multConfigChanged);
        connect(ui->measTime,  static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &MainWindow::multConfigChanged);
        connect(ui->format,    static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MainWindow::multConfigChanged);
        connect(ui->iSrcValue, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                this, &MainWindow::multConfigChanged);
        connect(ui->icpPhaseCor, &QCheckBox::stateChanged, this, &MainWindow::multConfigChanged);

        connect(ui->enAllChBox, &QCheckBox::stateChanged, this, &MainWindow::allChEnChanged);
    } else {
        disconnect(ui->adcFreq,   static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MainWindow::multConfigChanged);
        disconnect(ui->measTime,  static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &MainWindow::multConfigChanged);
        disconnect(ui->format,    static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MainWindow::multConfigChanged);
        disconnect(ui->iSrcValue, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                this, &MainWindow::multConfigChanged);
        disconnect(ui->icpPhaseCor, &QCheckBox::stateChanged, this, &MainWindow::multConfigChanged);

        disconnect(ui->enAllChBox, &QCheckBox::stateChanged, this, &MainWindow::allChEnChanged);
    }

}

void MainWindow::allChEnChanged(int state) {
    if (state != Qt::PartiallyChecked) {
        multConnectSignals(false);
        for (int ch=0; ch < m_chUi.size(); ch++) {
            m_chUi[ch].enBox->setCheckState((Qt::CheckState)state);
        }
        multConnectSignals(true);
    }
    multConfigChanged();
}


void MainWindow::closeEvent(QCloseEvent *event) {
    saveSettings();
    closeCurModule();
}

QSharedPointer<LQMeas::LTR25> MainWindow::curDev() {
    return qSharedPointerObjectCast<LQMeas::LTR25>(m_devsel->currentModule());
}
