#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>


namespace Ui {
class SettingsDialog;
}


struct MultSettings {
    int dc_prec;
    int ac_prec;
    int freq_prec;
    int snr_prec;
};


class MultSettingsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit MultSettingsDialog(MultSettings initSet, QWidget *parent = 0);
    ~MultSettingsDialog();
    
    MultSettings settings() const;
private:
    Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
